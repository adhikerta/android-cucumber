Feature: Sample testing for sample app

  Scenario: User starts sample app
    Given User is at home screen
    When User starts sample app
    Then home fragment is visible with text "This is home fragment"

  Scenario: User fills some data
    Given Sample app is running
    When User clicks side navigation button
    And User clicks "Gallery" menu item
    Then gallery fragment is visible
    And User fills the following data
      | Email          | Password | Type Password Again | I am a | Address                 |
      | niko@gmail.com | niko1234 | niko1234            | Male   | Strmečkog put 5, Zagreb |
    And notification message is visible with text "I am Cucumber on Android"