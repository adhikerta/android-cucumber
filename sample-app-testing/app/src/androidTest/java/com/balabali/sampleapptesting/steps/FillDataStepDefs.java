package com.balabali.sampleapptesting.steps;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import java.util.Map;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FillDataStepDefs extends BaseStepDefs {
  @Override
  public String getDefaultActivityPackage() {
    return "com.balabali.sample";
  }

  @Given("Sample app is running")
  public void sampleAppIsRunning() {
    assertEquals(mDevice.getCurrentPackageName(), getDefaultActivityPackage());
  }

  @When("User clicks side navigation button")
  public void userClicksSideNavigationButton() {
    // navigation button is an ImageButton with "Open navigation drawer" as content-desc
    getViewObject("com.balabali.sample:id/toolbar")
        .findObject(By.descContains("Open navigation drawer"))
        .click();
  }

  @And("User clicks {string} menu item")
  public void userClicksMenuItem(String menuText) {
    getViewObject("com.balabali.sample:id/design_navigation_view")
        .findObject(By.text(menuText))
        .click();
  }

  @Then("gallery fragment is visible")
  public void galleryFragmentIsVisible() {
    // wait until new fragment is visible and the toolbar title should be "Gallery"
    UiObject2 toolbar = getViewObject("com.balabali.sample:id/toolbar");
    toolbar.wait(Until.textContains("Gallery"), WAIT_TIMEOUT);
    assertNotNull(toolbar.findObject(By.text("Gallery")));
  }

  @And("User fills the following data")
  public void userFillsTheFollowingData(DataTable formData) {
    Map<String, String> data = formData.asMaps().get(0);

    UiObject2 email = getViewObject("com.balabali.sample:id/email");
    // lets try to make soft keyboard to be visible
    email.click();
    email.setText(data.get("Email"));
    // hide soft keyboard with this
    mDevice.pressBack();

    getViewObject("com.balabali.sample:id/password").setText(data.get("Password"));
    getViewObject("com.balabali.sample:id/confirm_password")
        .setText(data.get("Type Password Again"));

    // on smaller screen (mobile), Address and Save button will need to be scrolled into screen
    getViewObject("com.balabali.sample:id/address").setText(data.get("Address"));
    getViewObject("com.balabali.sample:id/saveButton").click();
  }

  @And("notification message is visible with text {string}")
  public void toastMessageIsVisibleWithText(String message) {
    UiObject snackbarTextView = mDevice.findObject(new UiSelector().text(message));
    try {
      assertEquals(snackbarTextView.getText(), message);
    } catch (UiObjectNotFoundException e) {
      e.printStackTrace();
    }
  }
}
