package com.balabali.sampleapptesting.steps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import org.junit.Assert;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNotNull;

/** Abstract base class for step definitions. */
public abstract class BaseStepDefs {

  protected UiDevice mDevice;
  public static final int LAUNCH_TIMEOUT = 5000;
  public static final int WAIT_TIMEOUT = 5000;

  public BaseStepDefs() {
    instantiateUiDevice();
  }

  public BaseStepDefs(UiDevice mDevice) {
    this.mDevice = mDevice;
  }

  /**
   * Get the default activity package name.
   *
   * @return String
   */
  public abstract String getDefaultActivityPackage();

  private void instantiateUiDevice() {
    if (mDevice == null)
      mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
  }

  /** Start the main activity from home screen. */
  public void startMainActivityFromHomeScreen() {
    // Initialize UiDevice instance
    instantiateUiDevice();

    // Start from the home screen
    mDevice.pressHome();

    // Wait for launcher
    final String launcherPackage = getLauncherPackageName();
    Assert.assertThat(launcherPackage, notNullValue());
    mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

    // Launch and wait for the activity to appear
    launchActivity(getDefaultActivityPackage());
  }

  /**
   * Uses package manager to find the package name of the device launcher. Usually this package is
   * "com.android.launcher" but can be different at times. This is a generic solution which works on
   * all platforms.`
   */
  private String getLauncherPackageName() {
    // Create launcher Intent
    final Intent intent = new Intent(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_HOME);

    // Use PackageManager to get the launcher package name
    PackageManager pm = getApplicationContext().getPackageManager();
    ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
    return resolveInfo.activityInfo.packageName;
  }

  /**
   * Launch an activity from the given {@code packageName}.
   *
   * @param packageName String of package name of the activity
   */
  public void launchActivity(String packageName) {
    Context context = getApplicationContext();
    final Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); // Clear out any previous instances?
    context.startActivity(intent);
    mDevice.wait(Until.hasObject(By.pkg(getDefaultActivityPackage()).depth(0)), LAUNCH_TIMEOUT);
  }

  /**
   * Get view object of the current UiObject2 instance
   *
   * @param resourceName name of the view object, e.g. "com.circleblue.ecr:id/authTitleTextView"
   * @return
   */
  public UiObject2 getViewObject(String resourceName) {
    instantiateUiDevice();
    UiObject2 view = mDevice.findObject(By.res(resourceName));
    if (view == null) {
      // if resourceName is correct, then it might be 2 problems:
      // 1. View element is not visible on the screen
      UiScrollable scrollable = new UiScrollable(new UiSelector().scrollable(true));

      try {
        scrollable.scrollIntoView(new UiSelector().resourceId(resourceName));
      } catch (UiObjectNotFoundException e) {
        //e.printStackTrace();
      }

      view = mDevice.findObject(By.res(resourceName));

      // 2. It is being obstructed, for ex: by soft keyboard. For this you have to do this manually
      // for ex by calling mDevice.pressBack();
    }

    return view;
  }

  /**
   * Get view object from the given resourceName and set text on it.
   *
   * @param resourceName
   * @param text
   */
  public void viewObjectSetText(String resourceName, String text) {
    UiObject2 view = getViewObject(resourceName);
    assertNotNull(view);
    view.setText(text);
  }
}
