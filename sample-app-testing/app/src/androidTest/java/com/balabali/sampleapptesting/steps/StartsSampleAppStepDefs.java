package com.balabali.sampleapptesting.steps;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.Until;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class StartsSampleAppStepDefs extends BaseStepDefs {
  @Override
  public String getDefaultActivityPackage() {
    return "com.balabali.sample";
  }

  @Given("User is at home screen")
  public void userIsAtHomeScreen() {
    if (mDevice == null)
      mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

    assertThat(mDevice, notNullValue());
    mDevice.pressHome();
  }

  @When("User starts sample app")
  public void userStartsSampleApp() {
    startMainActivityFromHomeScreen();
  }

  @Then("home fragment is visible with text {string}")
  public void homeFragmentIsVisibleWithText(String text) {
    getViewObject("com.balabali.sample:id/toolbar")
            .wait(Until.textContains("Home"), WAIT_TIMEOUT);
    assertEquals(getViewObject("com.balabali.sample:id/text_home").getText(), text);
  }
}
