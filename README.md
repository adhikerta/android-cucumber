# Black box Android Tests by using Cucumber + UIAutomator

## Prerequisite

There are 2 Android Studio project folders inside: `sample` and `sample-app-testing`.

`sample` folder contains the tested application, so you'll need to open this project separately first, build, and install the app on your device (emulator or real). Smaller screen is preferrable in order to show scrolling action when the view element is not being shown on the screen.

`sample-app-testing` is the Android tests project itself and it is designed to tests the installed `sample` app by using Cucumber + UIAutomator. Check the following configuration setup to run or debug the tests using Android Studio.

---

## Android Studio Setup

The following plugins will be needed for easier development for Cucumber tests:

- Cucumber for Java
- Gherkin

And make sure to disable _Substeps IntelliJ plugin_ as stated [here](https://stackoverflow.com/questions/45929639/i-keep-getting-the-error-unimplemented-substep-definition-in-intellij-with-cu) because it can generate errors on .feature files (and Gherkin syntax).

---

## Run/Debug Configurations

To run or debug tests using Android Studio, you'll need a tests configuration by clicking `Run - Edit Configurations...` menu, and create a new `Android Instrumented Tests`. On `General` tab, choose `app` for `Module` selection.

![Run configuration](config.png)

---

## Known Issue

You can not test Toast message by using UIAutomator, you'll need Espresso based test for this.
