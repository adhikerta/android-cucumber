package com.balabali.sample.ui.gallery;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.balabali.sample.R;
import com.google.android.material.snackbar.Snackbar;

public class GalleryFragment extends Fragment {

  private GalleryViewModel galleryViewModel;

  public View onCreateView(
      @NonNull LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
    galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
    View root = inflater.inflate(R.layout.fragment_gallery, container, false);
    final Button saveBtn = root.findViewById(R.id.saveButton);
    saveBtn.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            // Toast.makeText(getContext(), "Hello, World!", Toast.LENGTH_LONG).show();

            // new AlertDialog.Builder(container.getContext())
            //    .setMessage("I am Cucumber on Android")
            //    .setPositiveButton(
            //        "OK",
            //        new DialogInterface.OnClickListener() {
            //          @Override
            //          public void onClick(DialogInterface dialog, int which) {
            //            dialog.dismiss();
            //          }
            //        })
            //    .show();

            Snackbar snackbar = Snackbar.make(v, "I am Cucumber on Android", Snackbar.LENGTH_LONG);
            snackbar.show();
          }
        });

    return root;
  }
}
